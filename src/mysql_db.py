#!/usr/bin/env python
import time

import MySQLdb

from settings import mysql


class MySQL:
    error_code = ''

    _instance = None
    _conn = None
    _cur = None

    _TIMEOUT = 30
    _timecount = 0

    def __init__(self):
        try:
            self._conn = MySQLdb.connect(host=mysql.SERVER,
                                         port=mysql.PORT,
                                         user=mysql.USER,
                                         passwd=mysql.PASSWORD,
                                         db=mysql.DB,
                                         charset=mysql.CHARSET)
        except MySQLdb.Error, e:
            self.error_code = e.args[0]
            error_msg = 'MySQL error! ', e.args[0], e.args[1]
            print error_msg

            # If the preset time-out period has not elapsed, try the connection again
            if self._timecount < self._TIMEOUT:
                interval = 5
                self._timecount += interval
                time.sleep(interval)
                return self.__init__()
            else:
                raise Exception(error_msg)

        self._cur = self._conn.cursor()
        self._instance = MySQLdb

    def query(self, sql):
        # 'Execute the SELECT statement'
        try:
            self._cur.execute("SET NAMES utf8")
            result = self._cur.execute(sql)
        except MySQLdb.Error, e:
            self.error_code = e.args[0]
            print "Database error code:", e.args[0], e.args[1]
            result = False
        return result

    def update(self, sql):
        # 'Execute UPDATE and DELETE statements'
        try:
            self._cur.execute("SET NAMES utf8")
            result = self._cur.execute(sql)
            self._conn.commit()
        except MySQLdb.Error, e:
            self.error_code = e.args[0]
            print "Database error code:", e.args[0], e.args[1]
            result = False
        return result

    def insert(self, sql):
        # 'Execute the INSERT statement. If the primary key is a self-growing int, the newly generated ID is returned'
        try:
            self._cur.execute("SET NAMES utf8")
            self._cur.execute(sql)
            self._conn.commit()
            return self._conn.insert_id()
        except MySQLdb.Error, e:
            self.error_code = e.args[0]
            return False

    def fetchAllRows(self):
        # 'Returns a list of results'
        return self._cur.fetchall()

    def fetchOneRow(self):
        # 'Returns a row of results, and the cursor points to the next row. When the last line is reached, it returns None'
        return self._cur.fetchone()

    def getRowCount(self):
        # 'Gets the number of result rows'
        return self._cur.rowcount

    def commit(self):
        # 'Database commit operation'
        self._conn.commit()

    def rollback(self):
        # 'Database rollback operation'
        self._conn.rollback()

    def __del__(self):
        # 'Release resources (system GC auto-call)'
        try:
            self._cur.close()
            self._conn.close()
        except:
            pass

    def close(self):
        # 'Close the database connection'
        self.__del__()

    def check_table(self, table_name):
        # 'Check table exists'
        self.query('CHECK TABLE `' + table_name + '` ')
        for row in self.fetchAllRows():
            if row[2] == 'status' and row[3] == 'OK':
                return True

        return False

    def escape(self, string):
        return MySQLdb.escape_string(string)
