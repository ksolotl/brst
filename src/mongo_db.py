# import pymongo
#
# from brst.settings import mongo
#
#
# class MongoDBClient(object):
#     def __init__(self, col, index=None):
#         connection = pymongo.Connection(mongo.SERVER, mongo.PORT)
#         self.db = connection[mongo.DB]
#         self.collection = self.db[mongo.COLLECTION]
#         if index:
#             self.collection.create_index(index, unique=True)
#
#     def get_collection(self):
#         return self.collection
#
#     def _walk(self):
#         """
#         generator of all the documents in this collection
#         """
#         skip = 0
#         limit = 1000
#         hasMore = True
#         while hasMore:
#             res = self.collection.find(skip=skip, limit=limit)
#             hasMore = (res.count(with_limit_and_skip=True) == limit)
#             for x in res:
#                 yield x
#             skip += limit
#
#     def walk(self):
#         """
#         return all the documents in this collection
#         """
#         docs = []
#         for doc in self._walk():
#             docs.append(doc)
#         return docs

import pymongo

from scrapy.conf import settings
from scrapy.exceptions import DropItem
from scrapy import log
from settings import mongo


class MongoDBPipeline(object):
    def __init__(self):
        self.connection = pymongo.MongoClient(
            settings[mongo.SERVER],
            settings[mongo.PORT]
        )
        # db = connection[mongo.DB]
        # self.collection = db[mongo.COLLECTION]

    def process_item(self, item, collection_name):
        valid = True
        for data in item:
            if not data:
                valid = False
                raise DropItem("Missing {0}!".format(data))
        if valid:
            collection = self.connection[mongo.DB][collection_name]
            collection.insert(dict(item), True, False, True)
            # log.msg("Question added to MongoDB database!",
            #         level=log.DEBUG, spider=spider)
        return item

    def create_unique_index(self, collection_name, keys):
        collection = self.connection[mongo.DB][collection_name]
        index_keys = []
        for key in keys:
            index_keys.append((key, pymongo.DESCENDING))

        collection.create_index(
            index_keys,
            unique=True
        )

    def clean_collection(self, collection_name):
        collection = self.connection[mongo.DB][collection_name]
        collection.drop()

    def get_all(self, collection_name):
        collection = self.connection[mongo.DB][collection_name]
        return collection.find({})
