#!/usr/bin/env bash

sudo apt-get -y install python-pip
sudo apt-get install mysql-server mysql-client mongodb-server mongodb-clients
sudo mysql -uroot -e'CREATE DATABASE `brst` CHARACTER SET utf8 COLLATE utf8_general_ci;'
sudo mysql -uroot -e'CREATE USER "brst"@"localhost" IDENTIFIED BY "aeceiVigee9ieGha";'
sudo mysql -uroot -e'GRANT ALL PRIVILEGES ON brst.* TO "brst"@"localhost";'
sudo pip install scrapy
sudo pip install pymongo
sudo apt-get install python-dev libmysqlclient-dev
sudo pip install pymysql
sudo pip install MySQL-python
sudo pip install scrapy-random-useragent
sudo pip install scrapy_proxies

#mkdir -p ~/brst/modules
cd ~/brst/modules/scrapy-proxies
#git clone git@github.com:aivarsk/scrapy-proxies.git
sudo python setup.py install