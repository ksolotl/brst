# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html
import abc

import scrapy
from datetime import date, time, datetime

import sys

from src import mysql_db, mongo_db
import settings


class BrstItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class DBItem(scrapy.Item):
    COLLECTION_NAME = ''

    def prepare_table(self):
        if settings.MONGO_ON:
            self._mongo_create_table()
        if settings.MYSQL_ON:
            self._my_create_table()

    @abc.abstractmethod
    def _my_create_table(self):
        raise NotImplementedError("Please Implement this method")

    @abc.abstractmethod
    def _mongo_create_table(self):
        raise NotImplementedError("Please Implement this method")

    def _mongo_save(self):
        mongo = mongo_db.MongoDBPipeline()
        mongo.process_item(self, self.COLLECTION_NAME)

    @abc.abstractmethod
    def _my_save(self):
        raise NotImplementedError("Please Implement this method")

    def _my_clean_table(self):
        self._my_create_table()
        mysql = mysql_db.MySQL()
        mysql.query('TRUNCATE TABLE `' + self.COLLECTION_NAME + '`')

    def _mongo_clean_table(self):
        mongo = mongo_db.MongoDBPipeline()
        mongo.clean_collection(self.COLLECTION_NAME)

    @abc.abstractmethod
    def _mongo_get_all(self):
        raise NotImplementedError("Please Implement this method")

    @abc.abstractmethod
    def _my_get_all(self):
        raise NotImplementedError("Please Implement this method")

    def save(self):
        if settings.MONGO_ON:
            self._mongo_save()
        if settings.MYSQL_ON:
            self._my_save()

    def clean(self):
        if settings.MONGO_ON:
            self._mongo_clean_table()
        if settings.MYSQL_ON:
            self._my_clean_table()

    def get_all(self):
        if settings.MAIN_DB == 'mongo':
            return self._mongo_get_all()
        if settings.MAIN_DB == 'mysql':
            return self._my_get_all()


class ZapCategoryItem(DBItem):
    # define the fields for your item here like:
    COLLECTION_NAME = 'zap_cat'
    field = scrapy.Field()
    name = scrapy.Field()
    parent_field = scrapy.Field()
    parent_name = scrapy.Field()

    def prepare_table(self):
        self.clean()

    def _my_create_table(self):
        mysql = mysql_db.MySQL()
        if not mysql.check_table(self.COLLECTION_NAME):
            mysql.query(
                'CREATE TABLE `' + self.COLLECTION_NAME + '` (`id` INT UNSIGNED NOT NULL AUTO_INCREMENT, '
                + '`field` VARCHAR(32), `name` VARCHAR(32), `parent_field` VARCHAR(32), '
                + '`parent_name` VARCHAR(32), PRIMARY KEY (`id`)) ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;'
            )

    def _mongo_create_table(self):
        mongo_db.MongoDBPipeline().create_unique_index(self.COLLECTION_NAME, ['parent_name', 'name'])

    def _my_save(self):
        mysql = mysql_db.MySQL()
        mysql.query(
            'INSERT IGNORE INTO `' + self.COLLECTION_NAME + '` SET '
            + '`field`="' + self['field'] + '", `name`="' + self['name'] + '", `parent_field`="' + self['parent_field']
            + '", `parent_name`="' + self['parent_name'] + '" '

        )
        mysql.commit()

    def _mongo_get_all(self):
        mongo = mongo_db.MongoDBPipeline()
        collection = []
        categories = mongo.get_all(self.COLLECTION_NAME)
        print categories.count()
        if categories.count() > 0:
            for cat in categories:
                item = ZapCategoryItem()
                item['field'] = cat['field']
                item['name'] = cat['name']
                item['parent_field'] = cat['parent_field']
                item['parent_name'] = cat['parent_name']
                collection.append(item)

        return collection

    def _my_get_all(self):
        mysql = mysql_db.MySQL()
        collection = []
        mysql.query('SELECT * FROM `' + self.COLLECTION_NAME + '` LIMIT 1')  # todo remove limit
        categories = mysql.fetchAllRows()
        for cat in categories:
            item = ZapCategoryItem()
            item['field'] = cat[1]
            item['name'] = cat[2]
            item['parent_field'] = cat[3]
            item['parent_name'] = cat[4]
            collection.append(item)

        return collection


class ScrappedItem(DBItem):
    COLLECTION_NAME = 'item'
    website = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    link = scrapy.Field()
    raw_data = scrapy.Field()
    date = scrapy.Field()

    def _my_create_table(self):
        mysql = mysql_db.MySQL()
        if not mysql.check_table(self.COLLECTION_NAME):
            mysql.query(
                'CREATE TABLE `' + self.COLLECTION_NAME
                + '` ('
                  '`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,'
                  '`website` VARCHAR(32),'
                  '`title` VARCHAR(255),'
                  '`price` VARCHAR(32),'
                  '`link` VARCHAR(255),'
                  '`raw_data` BLOB,'
                  '`date` VARCHAR(32),'
                  'PRIMARY KEY (`id`),'
                  'UNIQUE INDEX (`website`, `link`)'
                  ') ENGINE=INNODB CHARSET=utf8 COLLATE=utf8_general_ci;'
            )

    def _mongo_create_table(self):
        mongo_db.MongoDBPipeline().create_unique_index(self.COLLECTION_NAME, ["website", "link"])

    def _my_save(self):
        mysql = mysql_db.MySQL()
        mysql.query(
            'INSERT IGNORE INTO `' + self.COLLECTION_NAME + '` SET '
            + '`website`="' + self['website'] + '", `title`="' + self['title'] + '", '
            + '`raw_data`="' + mysql.escape(self['raw_data']) + '", `price`="' + self['price'] + '", '
            + '`link`="' + self['link'] + '", `date`="' + self['date'] + '" '
            + ' ON DUPLICATE KEY UPDATE `title`="' + self['title'] + '", '
            + ' raw_data`="' + mysql.escape(self['raw_data']) + '", `price`="' + self['price'] + '", '
        )
        mysql.commit()

    def _mongo_get_all(self):
        mongo = mongo_db.MongoDBPipeline()
        collection = []
        items = mongo.get_all(self.COLLECTION_NAME)
        print items.count()
        if items.count() > 0:
            for item in items:
                scrapped_item = ScrappedItem
                scrapped_item['website'] = item['website']
                scrapped_item['title'] = item['title']
                scrapped_item['price'] = item['price']
                scrapped_item['link'] = item['link']
                scrapped_item['raw_data'] = item['raw_data']
                scrapped_item['date'] = item['date']
                collection.append(scrapped_item)

        return collection

    def _my_get_all(self):
        mysql = mysql_db.MySQL()
        collection = []
        mysql.query('SELECT * FROM `' + self.COLLECTION_NAME + '` ')
        items = mysql.fetchAllRows()
        for item in items:
            scrapped_item = ScrappedItem
            scrapped_item['website'] = item[1]
            scrapped_item['title'] = item[2]
            scrapped_item['price'] = item[3]
            scrapped_item['link'] = item[4]
            scrapped_item['raw_data'] = item[5]
            scrapped_item['date'] = item[6]
            collection.append(scrapped_item)

        return collection

    def save(self):
        sys.stdout.write('.=')
        if not self.get('date'):
            self['date'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            print self['date']

        super(ScrappedItem, self).save()
