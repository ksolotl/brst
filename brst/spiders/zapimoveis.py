# -*- coding: utf-8 -*-
import json
import re
import string

import time
from scrapy import FormRequest

from brst import items
from brst.spiders.abstract_crawler import AbstractSpider


class ZapimoveisSpider(AbstractSpider):
    name = "zapimoveis"
    allowed_domains = ["www.zapimoveis.com.br"]
    start_urls = []
    method = 'POST'
    handle_httpstatus_list = [200, 403, 400]
    # def __init__(self, **kwargs):
    #     super(ZapimoveisSpider, self).__init__(**kwargs)
    #     self._collect_start_urls()

    types = {
        'venda': 'Imovel',
        # 'aluguel': 'Imovel',
        # 'lancamentos': 'CampanhaImovel'
    }

    def errback(self, response):
        print ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'
        print response
        print ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'

    # Rules = (Rule(LinkExtractor(allow=(), restrict_xpaths=('//a[@class="button next"]',)), callback="parse", follow=True),)

    def parse(self, response):
        try:
            json_data = json.loads(response.body_as_unicode())
            # print json_data
        except Exception:
            print '======================'
            print response.body_as_unicode()
            print '======================'
            exit()

        print '============== page: ' + str(json_data['Resultado']['PaginaAtual']) + '==================='
        f = open('var/logs/' + str(time.time()) + '.json', 'w')
        try:
            f.write(response.body_as_unicode().encode('ascii', 'ignore'))  # python will convert \n to os.linesep
        except Exception :
            pass
        f.close()
        count_of_data = json_data['Resultado']['QuantidadeRegistros']
        count_on_page = json_data['Resultado']['Resultado'].__len__()
        print count_on_page, count_of_data, json_data['Resultado']['JsonLD']
        json_ld = json.loads(json_data['Resultado']['JsonLD'])
        if count_of_data > 0 and count_on_page:
            items.ScrappedItem().prepare_table()
            for result in json_data['Resultado']['Resultado']:
            # for result in json_ld:
                if not result.get('object'):
                    continue

                item = items.ScrappedItem()
                item['website'] = 'www.zapimoveis.com.br'
                # item['link'] = result['object']['url']
                item['link'] = result['UrlFicha']
                # item['title'] = result['object']['name']
                item['title'] = result['MensagemContatePadrao']
                # item['price'] = result['price']
                # item['price'] = re.sub(r'\D', '', result['Valor'])
                item['price'] = result['Valor']
                item['raw_data'] = json.dumps(result)
                item.save()

            # follow pagination link
            next_page_path = json_data['breadcrumb']['Url']
            path_parts = re.match('([\w-]+)/([\w-]+)/', next_page_path)
            tipo_oferta = self.types.get(path_parts.group(1), '1')
            form_data = {
                'tipoOferta': tipo_oferta,
                'paginaAtual': str(int(json_data['Resultado']['PaginaAtual']) + 1),
                'pathName': next_page_path,
                'hashFragment': json.dumps({
                    'pagina': '1',
                    'ordem': 'Relevancia',
                    'paginaOrigem': 'ResultadoBusca',
                    'semente': '656519183',
                    'formato': json_data['Formato']
                }),
                'formato': 'Lista'
            }
            request_body = json.dumps(form_data)
            yield FormRequest(
                'https://www.zapimoveis.com.br/Busca/RetornarBuscaAssincrona/',
                method=self.method,
                formdata=form_data,
                body=request_body,
                headers={
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'Referer': 'https://www.zapimoveis.com.br/' + next_page_path,
                    'User-Agent': self._random_user_agent(),
                },
                callback=self.parse,
                encoding='utf-8',
                errback=self.errback
            )

    @classmethod
    def handles_request(cls, request):
        return super(ZapimoveisSpider, cls).handles_request(request)

    def _collect_start_urls(self):
        pass

    def start_requests(self):
        categories = items.ZapCategoryItem().get_all()
        for cat in categories:
            print cat
            part = self._get_path_part(cat['name'])
            print part
            for type, type_id in self.types.iteritems():
                form_data = {
                    'tipoOferta': type_id,
                    'paginaAtual': '1',
                    'pathName': '/' + type + '/' + part + '/brasil/',
                    'hashFragment': json.dumps({
                        'pagina': '1',
                        'ordem': 'Relevancia',
                        'paginaOrigem': 'ResultadoBusca',
                        'semente': '656519183',
                        'formato': 'Lista'
                    }),
                    'formato': 'Lista'
                }
                request_body = json.dumps(form_data)
                yield FormRequest(
                    'https://www.zapimoveis.com.br/Busca/RetornarBuscaAssincrona/',
                    method=self.method,
                    formdata=form_data,
                    body=request_body,
                    headers={
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                        'Referer': 'https://www.zapimoveis.com.br/' + type + '/' + part + '/',
                        'User-Agent': self._random_user_agent(),
                    },
                    callback=self.parse,
                    encoding='utf-8',
                    errback=self.errback
                )
                # exit()

    def _get_path_part(self, part):
        part = part.encode("utf-8")
        part = string.lower(part)
        part = re.sub(r"(?u)\W", '-', part)
        part = {
            u'apartamento padrao': 'apartamentos',
            u'apartamento-padrao': 'apartamentos',
            u'apartamento padrão': 'apartamentos',
            u'apartamento-padrão': 'apartamentos',
            u'casa padrao': 'casas',
            u'casa-padrao': 'casas',
            u'casa padrão': 'casas',
            u'casa-padrão': 'casas',
            u'terreno padrao': 'terrenos',
            u'terreno-padrao': 'terrenos',
            u'terreno padrão': 'terrenos',
            u'terreno-padrão': 'terrenos',
        }.get(part, part)

        return part

