import random

import scrapy

from brst import settings


class AbstractSpider(scrapy.Spider):
    user_agent_list = []

    def _random_user_agent(self):
        if len(self.user_agent_list) == 0:
            with open(settings.USER_AGENT_LIST, 'r') as f:
                self.user_agent_list = [line.strip() for line in f.readlines()]

        return random.choice(self.user_agent_list)
