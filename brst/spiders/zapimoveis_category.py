# -*- coding: utf-8 -*-
import scrapy
import json

from scrapy import FormRequest

from brst import items
from brst.spiders.abstract_crawler import AbstractSpider


class ZapimoveisCategorySpider(AbstractSpider):
    name = "zapimoveis_category"
    allowed_domains = ["www.zapimoveis.com.br"]
    start_urls = [
        'https://www.zapimoveis.com.br/busca/RetornarGrupoNavegadoresImovel?navegador=subtipoimovel&gruponavegador=nomeagrupamentotipoimovel%2Csubtipoimovel_facet&transacao=Venda&tipoOferta=Imovel']
    # handle_httpstatus_list = [403]
    handle_httpstatus_list = [200, 403, 400]

    def errback(self, response):
        print ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'
        print response
        print ';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;'

    def parse(self, response):
        # print '---------------------------'
        # print response
        # print response.body_as_unicode
        # print '---------------------------'
        jsonresponse = json.loads(response.body_as_unicode())
        items.ZapCategoryItem().prepare_table()
        for parent in jsonresponse[0]['GrupoModificadores']:
            parent_name = parent['Valor']
            parent_field = parent['Campo']
            for child in parent['GrupoModificadores']:
                item = items.ZapCategoryItem()
                item['parent_field'] = parent_field
                item['parent_name'] = parent_name
                item['field'] = child['Campo']
                item['name'] = child['Valor']

                print item['name']
                item.save()

    def start_requests(self):
        for url in self.start_urls:
            yield FormRequest(
                url,
                method='GET',
                headers={
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    'User-Agent': self._random_user_agent(),
                },
                callback=self.parse,
                encoding='utf-8'
                # errback=self.errback
            )
